import os

path = os.getcwd()
files = os.listdir(path)

for index, file in enumerate(files):
    if file.endswith('.py'):
      continue

    os.rename(os.path.join(path, file), os.path.join(path, file.replace('br_', 'nc_')))