/**
 * deno run --allow-net --no-check --allow-read https://nightcitystudios.ml/server.js
 */
import opine from 'https://deno.land/x/opine@1.1.0/mod.ts'

const app = opine()

app.get('/getstream/:channel', (req, res) => {

  console.log(`Get stream ${req.params.channel}`)

  let playlist = '#EXTM3U'
  for (let i = 0; i < 1; i++) {
    playlist += '\n#EXTINF:-1,'
    playlist += `\nhttp://srv.maxptv.net:8080/Liza/1234/${req.params.channel}`
  }
  
  res.type('application/x-mpegURL')
  res.send(playlist)
})


app.get('/getchannel/:channel', (req, res) => {

  

  res.type('application/x-mpegURL')
  res.send(`#EXTM3U
#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=1558000
/getstream/${req.params.channel}`)
})


app.listen(9980, () => console.log('Running'))
